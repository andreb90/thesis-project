# README #

This is the repository used to save the thesis project: creation of a service that allow you to build a custom page and a basic we chat client,  both using Web Components standard. 

### Dependencies ###

* NodeJS
* Bower (nodeJs package) 
* MYSQL (used only for webchat)

### Instructions ###

* Clone project 
* npm install
* bower install
* edit database configuration (config/env/development.js)
* node server.js